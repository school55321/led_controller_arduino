#define ARCH_ARM 1

#define TOTAL_LEDS 3601
unsigned selectedLed = 0;
unsigned char ledsData[TOTAL_LEDS];

#ifdef ARCH_ARM
#define PORT4_PORR 0x40040088
#define PORT4_POSR 0x4004008A

const unsigned POSR = PORT4_POSR;

void sendBit(bool high) {
  unsigned r0;
  unsigned r1;
  if (high) {
    asm volatile(
      "MOV %[reg0],#0x400 \n\t" // Set 0x400 (1 << 10) into R0
      "MOVW %[reg1],#0x0080 \n\t"
      "MOVT %[reg1],#0x4004 \n\t"
      "STR %[reg0],[%[reg1], #10] \n\t" // STORE value of R0 into 'RAM' at address that is in R1 (Set 1024 into POSR => GPIO set to HIGH)
      ".rept 30 \n\t" // Do 40 times NOP => 30 * ~21ns -> 625ns
      "NOP \n\t"
      ".endr \n\t"


      "MOV %[reg0],#0x4000000 \n\t" // Set 0x400 (1 << 10) into R0
      "STR %[reg0],[%[reg1], #8] \n\t" // STORE value of R0 into 'RAM' at address that is in R1 (Set 1024 into POSR => GPIO set to HIGH)
      ".rept 16 \n\t" // Do 16 time NOP => 40 * ~21ns -> 333ns
      "NOP \n\t"
      ".endr \n\t"
      : 
      [reg0] "=&r" (r0),
      [reg1] "=&r" (r1)
      );
  } else {
    asm volatile(
      "MOV %[reg0],#0x400 \n\t" // Set 0x400 (1 << 10) into R0
      "MOVW %[reg1],#0x0080 \n\t"
      "MOVT %[reg1],#0x4004 \n\t"
      "STR %[reg0],[%[reg1], #10] \n\t" // STORE value of R0 into 'RAM' at address that is in R1 (Set 1024 into POSR => GPIO set to HIGH)
      ".rept 8 \n\t" // Do 40 time NOP => 40 * ~21ns -> 833ns
      "NOP \n\t"
      ".endr \n\t"


      "MOV %[reg0],#0x4000000 \n\t" // Set 0x400 (1 << 10) into R0
      "STR %[reg0],[%[reg1], #8] \n\t" // STORE value of R0 into 'RAM' at address that is in R1 (Set 1024 into POSR => GPIO set to HIGH)
      ".rept 28 \n\t" // Do 40 time NOP => 40 * ~21ns -> 833ns
      "NOP \n\t"
      ".endr \n\t"
      : 
      [reg0] "=&r" (r0),
      [reg1] "=&r" (r1)
      );
  }
}

inline void sendByteAsm(unsigned char r, unsigned char g, unsigned char b, unsigned char w) {
  noInterrupts();
  unsigned int ledsValue = (r << 24) | (g << 16) | (b << 8) | w;
  for (unsigned i = 0; i < 32; i++) {
    if ((ledsValue & 0x80000000) == 0x80000000) sendBit(true);
    else sendBit(false);
    ledsValue <<= 1;
  }
  interrupts();
}
#else
#define PIXEL_PORT PORTB  // Port of the pin the pixels are connected to
#define PIXEL_DDR DDRB    // Port of the pin the pixels are connected to
#define PIXEL_BIT 4       // Bit of the pin the pixels are connected to

inline void sendByteAsm(unsigned char r, unsigned char g, unsigned char b, unsigned char w) {
  asm volatile(
    "ldi r16, 0x01 \n\t"  // Load our counter
    "bitsend: \n\t"
    "sbi %[port], %[bit] \n\t"  // (2) Set output (HIGH)
    "sbrc %[data], 7 \n\t"      // (2-3) check if bit 7 is clear (0) => long wait first
    "jmp bithigh \n\t"          // (3) jump to 1 byte timings
    "cbi %[port], %[bit] \n\t"  // (2) max 6 CLK, Clear the output bit (LOW)
    "lsl %[data] \n\t"          // (1) left shift
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "brcs end \n\t"             // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend \n\t"          // (3) jump back
    "bithigh: \n\t"             // Code for when 1 must send (10 clk high, 6 clk low)
    "lsl %[data] \n\t"          // (1) left shift, now we still have some time
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"
    "cbi %[port], %[bit] \n\t"  // (2) Clear the output bit (LOW)
    "brcs end \n\t"             // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend \n\t"          // (3) jump back
    "end: \n\t" ::
      [port] "I"(_SFR_IO_ADDR(PIXEL_PORT)),
    [bit] "I"(PIXEL_BIT),
    [data] "r"(r));


  asm volatile(
    "ldi r16, 0x01 \n\t"  // Load our counter
    "bitsend1: \n\t"
    "sbi %[port], %[bit] \n\t"  // (2) Set output (HIGH)
    "sbrc %[data1], 7 \n\t"     // (2-3) check if bit 7 is clear (0) => long wait first
    "jmp bithigh1 \n\t"         // (3) jump to 1 byte timings
    "cbi %[port], %[bit] \n\t"  // (2) max 6 CLK, Clear the output bit (LOW)
    "lsl %[data1] \n\t"         // (1) left shift
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "brcs end1 \n\t"            // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend1 \n\t"         // (3) jump back
    "bithigh1: \n\t"            // Code for when 1 must send (10 clk high, 6 clk low)
    "lsl %[data1] \n\t"         // (1) left shift, now we still have some time
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"
    "cbi %[port], %[bit] \n\t"  // (2) Clear the output bit (LOW)
    "brcs end1 \n\t"            // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend1 \n\t"         // (3) jump back
    "end1: \n\t" ::
      [port] "I"(_SFR_IO_ADDR(PIXEL_PORT)),
    [bit] "I"(PIXEL_BIT),
    [data1] "r"(g));


  asm volatile(
    "ldi r16, 0x01 \n\t"  // Load our counter
    "bitsend2: \n\t"
    "sbi %[port], %[bit] \n\t"  // (2) Set output (HIGH)
    "sbrc %[data2], 7 \n\t"     // (2-3) check if bit 7 is clear (0) => long wait first
    "jmp bithigh2 \n\t"         // (3) jump to 1 byte timings
    "cbi %[port], %[bit] \n\t"  // (2) max 6 CLK, Clear the output bit (LOW)
    "lsl %[data2] \n\t"         // (1) left shift
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "brcs end2 \n\t"            // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend2 \n\t"         // (3) jump back
    "bithigh2: \n\t"            // Code for when 1 must send (10 clk high, 6 clk low)
    "lsl %[data2] \n\t"         // (1) left shift, now we still have some time
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"
    "cbi %[port], %[bit] \n\t"  // (2) Clear the output bit (LOW)
    "brcs end2 \n\t"            // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend2 \n\t"         // (3) jump back
    "end2: \n\t" ::
      [port] "I"(_SFR_IO_ADDR(PIXEL_PORT)),
    [bit] "I"(PIXEL_BIT),
    [data2] "r"(b));


  asm volatile(
    "ldi r16, 0x01 \n\t"  // Load our counter
    "bitsend3: \n\t"
    "sbi %[port], %[bit] \n\t"  // (2) Set output (HIGH)
    "sbrc %[data3], 7 \n\t"     // (2-3) check if bit 7 is clear (0) => long wait first
    "jmp bithigh3 \n\t"         // (3) jump to 1 byte timings
    "cbi %[port], %[bit] \n\t"  // (2) max 6 CLK, Clear the output bit (LOW)
    "lsl %[data3] \n\t"         // (1) left shift
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "nop \n\t"                  // do nops for remainging low time => about 10 clk low total
    "brcs end3 \n\t"            // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend3 \n\t"         // (3) jump back
    "bithigh3: \n\t"            // Code for when 1 must send (10 clk high, 6 clk low)
    "lsl %[data3] \n\t"         // (1) left shift, now we still have some time
    "rol r16 \n\t"              // (1) roll to left (counter)
    "nop \n\t"
    "cbi %[port], %[bit] \n\t"  // (2) Clear the output bit (LOW)
    "brcs end3 \n\t"            // (1-2) check if carry is set (1) => bit has rotated whole byte and loop is done
    "jmp bitsend3 \n\t"         // (3) jump back
    "end3: \n\t"
    ".rept 0 \n\t"
    "nop \n\t"
    ".endr \n\t" ::
      [port] "I"(_SFR_IO_ADDR(PIXEL_PORT)),
    [bit] "I"(PIXEL_BIT),
    [data3] "r"(w));
}
#endif

void setup_led() {
#ifdef ARCH_ARM
  pinMode(12, OUTPUT);
#else
  bitSet(PIXEL_DDR, PIXEL_BIT);
#endif
}

inline void setPixel(unsigned index, unsigned char r, unsigned char g, unsigned char b, unsigned char w) {
  if (index * 4 + 4 >= TOTAL_LEDS) return;

  ledsData[index * 4] = g;
  ledsData[index * 4 + 1] = r;
  ledsData[index * 4 + 2] = b;
  ledsData[index * 4 + 3] = w;
  selectedLed = max(index * 4 + 4, selectedLed);
}

inline void setPixel(unsigned char r, unsigned char g, unsigned char b, unsigned char w) {
  ledsData[selectedLed] = g;
  ledsData[selectedLed + 1] = r;
  ledsData[selectedLed + 2] = b;
  ledsData[selectedLed + 3] = w;
  
  selectedLed = selectedLed + 4;
  if (selectedLed >= TOTAL_LEDS - 4) {
    show();
    selectedLed = 0;
  }
}

void show() {
  for (unsigned i = 0; i < selectedLed; i += 4) {
    sendByteAsm(ledsData[i], ledsData[i + 1], ledsData[i + 2], ledsData[i + 3]);
    // LOGV(i);
    // LOG(": ");
    // log_rbgw(&ledsData[i]);
    // LOG_LN("");
  }
  
  selectedLed = 0;
}
