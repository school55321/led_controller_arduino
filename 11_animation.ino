unsigned char mode = 0;
unsigned int timer = 0;
int counter = 0;
unsigned char led_buff0[4];
unsigned char led_buff1[4];
unsigned char led_buff2[4];
unsigned char led_buff3[4];
int rgbw_process_buffer1[4];
int rgbw_process_buffer2[4];
int number_buffs[10];

#define GLOW_STEPS 32
#define WALK_STEPS 8
#define MODE_ALL_OFF 0
#define MODE_ALL_ON 1
#define MODE_GLOW_UP 2
#define MODE_GLOW_DOWN 3
#define MODE_WALK_UP 4

#define PATTERN1_START 0
#define PATTERN1_END 1
#define PATTERN2_START 2
#define PATTERN2_END 3
#define PATTERN3_START 4
#define PATTERN3_END 5
#define PATTERN4_START 6
#define PATTERN4_END 7

inline void cpy_rgbw(const unsigned char src[4], unsigned char dest[4]) {
  std::copy(&src[0], &src[4], dest);
}

inline void log_rbgw(const unsigned char rgbw[4]) {
  LOG("RBGW(");
  LOGV(rgbw[0]);
  LOG(",");
  LOGV(rgbw[1]);
  LOG(",");
  LOGV(rgbw[2]);
  LOG(",");
  LOGV(rgbw[3]);
  LOG(")");
}

void all_off() {
  set_frame(0, 0, 0, 0);
  show();
  mode = MODE_ALL_OFF;
}

void test() {
  for (unsigned i = 0; i < leds; i++) {
    setPixel(
      (i & 0x07) == 0 ? 255 : 0,
      (i & 0x07) == 1 ? 255 : 0,
      (i & 0x07) == 2 ? 255 : 0,
      (i & 0x07) == 7 ? 255 : (i & 0x07) >= 3 ? (i & 0x07) * 80 - 240 : 0); // 3, 4, 5, 6, 7
  }
  show();
  mode = MODE_ALL_OFF;
}

void all_on(unsigned char r, unsigned char g, unsigned char b, unsigned char w) {
  set_frame(r, g, b, w);
  show();
  mode = MODE_ALL_ON;
}

void set_frame(unsigned char r, unsigned char g, unsigned char b, unsigned char w) {
  for (unsigned i = 0; i < leds; i++) {
    setPixel(r, g, b, w);
  }
}

void glow(const unsigned char default_rgbw[4], const unsigned char second_rgbw[4]) {
  cpy_rgbw(default_rgbw, led_buff0);
  cpy_rgbw(second_rgbw, led_buff1);

  for (char i = 0; i < 4; i++) {
    rgbw_process_buffer1[i] = default_rgbw[i];
    rgbw_process_buffer2[i] = (second_rgbw[i] * 8 - default_rgbw[i] * 8) / GLOW_STEPS;
  }
  timer = 0;

  all_on(default_rgbw[0], default_rgbw[1], default_rgbw[2], default_rgbw[3]);
  mode = MODE_GLOW_UP;
}

void walk(const unsigned char default_rgbw[4], const unsigned char highlight_rgbw[4]) {
  cpy_rgbw(default_rgbw, led_buff0);
  cpy_rgbw(highlight_rgbw, led_buff1);

  for (char i = 0; i < 4; i++) {
    rgbw_process_buffer1[i] = default_rgbw[i];
    rgbw_process_buffer2[i] = (highlight_rgbw[i] * 8 - default_rgbw[i] * 8) / WALK_STEPS;
  }
  timer = 0;
  counter = 0;

  all_on(default_rgbw[0], default_rgbw[1], default_rgbw[2], default_rgbw[3]);
  mode = MODE_WALK_UP;
}

void add_pattern(const unsigned char pattern, const unsigned char rgbw[4], const int start, const int end) {
  if (pattern > 3) return;
  number_buffs[pattern * 2] = start;
  number_buffs[pattern * 2 + 1] = end;

  switch (pattern) {
    case 0:
      cpy_rgbw(rgbw, led_buff0);
      return;
    case 1:
      cpy_rgbw(rgbw, led_buff1);
      return;
    case 2:
      cpy_rgbw(rgbw, led_buff2);
      return;
    case 3:
      cpy_rgbw(rgbw, led_buff3);
      return;
  }
}

void show_pattern() {
  unsigned offset = 0;

  for (unsigned i = 0; i < leds; i++) {
    unsigned at_pattern = i - offset;
    if (at_pattern >= number_buffs[PATTERN4_END]) {
      offset = i;
      at_pattern = 0;
    }

    // LOGV(i);
    // LOG(": ");
    if (at_pattern >= number_buffs[PATTERN1_START] && at_pattern < number_buffs[PATTERN1_END]) {
      // First pattern
      setPixel(i, led_buff0[0], led_buff0[1], led_buff0[2], led_buff0[3]);
      // LOG("P0 -> ");
      // log_rbgw(led_buff0);
    } else if (at_pattern >= number_buffs[PATTERN2_START] && at_pattern < number_buffs[PATTERN2_END]) {
      // Second pattern
      setPixel(i, led_buff1[0], led_buff1[1], led_buff1[2], led_buff1[3]);
      // LOG("P1 -> ");
      // log_rbgw(led_buff1);
    } else if (at_pattern >= number_buffs[PATTERN3_START] && at_pattern < number_buffs[PATTERN3_END]) {
      // Third pattern
      setPixel(i, led_buff2[0], led_buff2[1], led_buff2[2], led_buff2[3]);
      // LOG("P2 -> ");
      // log_rbgw(led_buff2);
    } else if (at_pattern >= number_buffs[PATTERN4_START] && at_pattern < number_buffs[PATTERN4_END]) {
      // Fourth pattern
      setPixel(i, led_buff3[0], led_buff3[1], led_buff3[2], led_buff3[3]);
      // LOG("P3 -> ");
      // log_rbgw(led_buff3);
    } else {
      // LOG("X -> ");
      setPixel(i, 0, 0, 0, 0);
    }
    // LOG_LN("");
  }
  // LOG("SHOWING");
  mode = MODE_ALL_ON;
  show();
}


void glow_tick() {
  bool allDone = false;
  if (timer++ < speed) return;
  timer = 0;

  if (mode == MODE_GLOW_UP) {
    for (char i = 0; i < 4; i++) {
      if (rgbw_process_buffer2[i] > 0) {
        const int stepSize = max(rgbw_process_buffer2[i] / 8, 1);
        rgbw_process_buffer1[i] = min(rgbw_process_buffer1[i] + stepSize, led_buff1[i]);
      } else {
        const int stepSize = min(rgbw_process_buffer2[i] / 8, -1);
        rgbw_process_buffer1[i] = max(rgbw_process_buffer1[i] + stepSize, led_buff1[i]);
      }
      allDone |= allDone || (rgbw_process_buffer1[i] == led_buff1[i] && led_buff0[i] != led_buff1[i]);
    }

    if (allDone) mode = MODE_GLOW_DOWN;
  } else {
    for (char i = 0; i < 4; i++) {
      const int stepSize = max(rgbw_process_buffer2[i] / 8, 1);
      
      if (rgbw_process_buffer2[i] > 0) {
        const int stepSize = max(rgbw_process_buffer2[i] / 8, 1);
        rgbw_process_buffer1[i] = max(rgbw_process_buffer1[i] - stepSize, led_buff0[i]);
      } else {
        const int stepSize = min(rgbw_process_buffer2[i] / 8, -1);
        rgbw_process_buffer1[i] = min(rgbw_process_buffer1[i] - stepSize, led_buff0[i]);
      }
      allDone = allDone || (rgbw_process_buffer1[i] == led_buff0[i] && led_buff0[i] != led_buff1[i]);
    }

    if (allDone) mode = MODE_GLOW_UP;
  }


  set_frame(rgbw_process_buffer1[0], rgbw_process_buffer1[1], rgbw_process_buffer1[2], rgbw_process_buffer1[3]);
  show();
}

void walk_tick() {
  bool allDone = false;
  int add[4];
  if (timer++ < speed) return;
  timer = 0;

  for (char i = 0; i < 4; i++) {
    rgbw_process_buffer1[i] = led_buff0[i];
    add[i] = rgbw_process_buffer2[i] / 8;
  }

  const int lastLed = leds;
  for (int led = -WALK_STEPS * 2; led < lastLed + WALK_STEPS * 2; led++) {
    if (led + WALK_STEPS == leds / 2 + counter) {
      allDone = false;
      for (char i = 0; i < 4; i++) {
        rgbw_process_buffer1[i] = led_buff0[i];
      }
    }

    if ((led + WALK_STEPS >= counter && !allDone) || (led + WALK_STEPS - counter > lastLed)) {
      for (char i = 0; i < 4; i++) {
        if (add[i] > 0) {
          rgbw_process_buffer1[i] = min(rgbw_process_buffer1[i] + add[i], led_buff1[i]);
        } else {
          rgbw_process_buffer1[i] = max(rgbw_process_buffer1[i] + add[i], led_buff1[i]);
        }
        allDone |= allDone || (rgbw_process_buffer1[i] == led_buff1[i] && led_buff0[i] != led_buff1[i]);
      }

      if (led >= 0 && led < lastLed) setPixel(rgbw_process_buffer1[0], rgbw_process_buffer1[1], rgbw_process_buffer1[2], rgbw_process_buffer1[3]);
    } else if (led >= 0 && led < lastLed) {
      setPixel(led_buff0[0], led_buff0[1], led_buff0[2], led_buff0[3]);
    }
  }

  // if (++counter >= leds) counter = 0;
  if (++counter >= leds / 2) counter = 0;
  show();
}

void tick_animation() {
  switch (mode) {
    case MODE_GLOW_UP:
    case MODE_GLOW_DOWN:
      glow_tick();
      return;
    case MODE_WALK_UP:
      walk_tick();
      return;
    default:
      return;
  }
}