
#define ESP_SET_UART(BAUD, DATABITS, STOPBIT, PARITY) ("AT+UART_DEF=" BAUD "," DATABITS "," STOPBIT "," PARITY ",0")
#define ESP_RESET() ("AT+RST")
#define ESP_FIRMWARE_VERSION() ("AT+GMR")
#define ESP_SET_MODE(MODE) ("AT+CWMODE=" MODE)
#define ESP_MODE_AP_AND_STA "3"
#define ESP_MULTI_CONNECTION(ENABLE) (ENABLE ? "AT+CIPMUX=1" : "AT+CIPMUX=0")
#define ESP_START_SERVER(PORT) ("AT+CIPSERVER=1," PORT)
#define ESP_WIFI_SETUP(SSID, PASS) ("AT+CWJAP=\"" SSID "\",\"" PASS "\"")

void fail() {
  Serial.println(F("Failed"));
  while (1)
    ;
}

void esp_flush() {
  while (espSerial.available() > 0) {
    while (espSerial.available() > 0) espSerial.read();
    delay(1);
  }
}

String esp_read() {
  return espSerial.readString();
}

bool esp_command(const char *command, char pauseMux = 2, unsigned char bufferSize = 0, bool print = false) {
  bool prevO = false;
  unsigned char expectedDataSize = max(strlen(command) * 2, bufferSize * 2);
  int waitTime;
  int counter = 0;
  if (pauseMux > 0) waitTime = expectedDataSize * pauseMux;
  else waitTime = expectedDataSize >> (pauseMux * -1);

  esp_flush();

  if (print) {
    Serial.print(F("Sending: "));
    Serial.println(command);
  }

  espSerial.println(command);
  delay(waitTime);
  if (print) Serial.println(F("Receiving..."));

  String received = esp_read();
  if (print) Serial.println(received);
  return received.indexOf("OK") >= 0;
}

void esp_send_data(const String &pipe, const String data) {
  String command = "AT+CIPSEND=" + pipe + "," + String(data.length() + 2) + "\r\n";
  esp_command(command.c_str());
  esp_command((data + "\n").c_str());
}

void esp_setup_server() {
  Serial.print(F("\t\tEnable multi connection: "));
  Serial.println(esp_command(ESP_MULTI_CONNECTION(true)));

  Serial.print(F("\t\tStart server: "));
  Serial.println(esp_command(ESP_START_SERVER("8080")));
}

/***
This will setup the ESP permanent settings, like the baudrate
**/
void install_esp() {
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);
  delay(1);
  digitalWrite(8, HIGH);
  espSerial.begin(115200);
  espSerial.setTimeout(20);

  delay(1000);
  esp_flush();
  if (!esp_command(ESP_FIRMWARE_VERSION(), 2, 0, true)) {
    LOG("FAILED INSTALL");
    while (1) asm volatile("nop");
  }
  LOG("SETUP: ");
  LOGV_LN(esp_command(ESP_SET_UART("9600", "8", "1", "0")));

  // espSerial.println(ESP_FIRMWARE_VERSION());
  // delay(2);
  LOG("DONE");
  while (1) asm volatile("nop");
}

void esp_manual() {
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);
  delay(1);
  digitalWrite(8, HIGH);
  espSerial.begin(9600);
  espSerial.setTimeout(20);

  while (1) {
    if (Serial.available()) espSerial.write(Serial.read());
    if (espSerial.available()) Serial.write(espSerial.read());
  }
}

void setup_esp() {
  Serial.println(F("\tESP 01S setup..."));
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);
  delay(1);
  digitalWrite(8, HIGH);
  espSerial.begin(9600);
  espSerial.setTimeout(20);

  // Give time to startup, then flush buffer
  delay(1000);
  esp_flush();
  
  Serial.print(F("\t\tCheck system: "));
  Serial.println(esp_command("AT"));

  // One time -> will be saved to non-volatile memory
  Serial.print(F("\t\tSetting ESP mode: "));
  Serial.println(esp_command(ESP_SET_MODE(ESP_MODE_AP_AND_STA)));

  Serial.print(F("\t\tSetting connection: "));
  Serial.println(esp_command(ESP_WIFI_SETUP("VV_IoT", "WorkWithTheBest")));

  delay(5000);
  esp_setup_server();
}
