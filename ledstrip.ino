#include <SoftwareSerial.h>

#define ENABLE_LOGS 1

SoftwareSerial espSerial(2, 4);  // RX, TX
bool hasWifi = false;
bool hasIP = false;
unsigned int speed = 0;
unsigned leds = 600;

#define STR_HAS(STRING, SUBSTRING) (STRING.indexOf(SUBSTRING) >= 0)

#if ENABLE_LOGS
#define LOG(MSG) Serial.print(F(MSG))
#define LOGV(MSG) Serial.print(MSG)
#define LOG_LN(MSG) Serial.println(F(MSG))
#define LOGV_LN(MSG) Serial.println(MSG)
#else
#define LOG(MSG)
#define LOGV(MSG)
#define LOG_LN(MSG)
#define LOGV_LN(MSG)
#endif

void setup() {
  Serial.begin(9600);
  Serial.println(F("Starting up..."));
  // install_esp();
  // esp_manual();
  setup_led();
  setup_esp();

  // leds = 600;
  all_off();
  delay(1); // Give some space after sending data to leds. 

  leds = 80;
  const unsigned char start[4] = { 0, 1, 1, 1 };
  const unsigned char end[4] = { 150, 1, 1, 1 };
  // test();
  // walk(start, end);
  // glow(start, end);
  // all_on(100, 1, 1, 1);

  const unsigned char p0[4] = { 0, 255, 0, 0 };
  const unsigned char p1[4] = { 255, 0, 0, 0 };
  const unsigned char p2[4] = { 0, 0, 255, 0 };
  const unsigned char p3[4] = { 0, 0, 0, 255 };
  add_pattern(0, p0, 0, 10);
  add_pattern(1, p1, 10, 20);
  add_pattern(2, p2, 20, 30);
  add_pattern(3, p3, 30, 40);
  show_pattern();

  Serial.println(F("\nDone"));
}

void on_network_message(const String &espMessage) {
  const char pipeAt = espMessage.indexOf(',');
  const char lenghtAt = espMessage.indexOf(',', pipeAt + 1);
  const char messageAt = espMessage.indexOf(':', lenghtAt + 1);
  const String pipe = espMessage.substring(pipeAt + 1, pipeAt + 2);
  const int length = espMessage.substring(lenghtAt + 1, messageAt).toInt();
  const String message = espMessage.substring(messageAt + 1, messageAt + 1 + length);

  // WALK100000000000,255000255000
  LOG("Message from ");
  LOGV(pipe);
  LOG(" with ");
  LOGV(length);
  LOG(" characters: ");
  LOGV_LN(message);
  if (message.startsWith("OFF")) {
    all_off();
  } else if (message.startsWith("ALL")) {
    all_on(
      message.substring(3, 6).toInt(),
      message.substring(6, 9).toInt(),
      message.substring(9, 12).toInt(),
      message.substring(12, 15).toInt());
  } else if (message.startsWith("SPEED")) {
    speed = message.substring(5).toInt();
  } else if (message.startsWith("LEDS")) {
    leds = message.substring(4).toInt();
  } else if (message.startsWith("GLOW")) {
    unsigned char start[4] = { message.substring(4, 7).toInt(),
                               message.substring(7, 10).toInt(),
                               message.substring(10, 13).toInt(),
                               message.substring(13, 16).toInt() };
    unsigned char end[4] = { message.substring(17, 20).toInt(),
                             message.substring(20, 23).toInt(),
                             message.substring(23, 26).toInt(),
                             message.substring(26, 29).toInt() };
    glow(start, end);
  } else if (message.startsWith("WALK")) {
    unsigned char start[4] = { message.substring(4, 7).toInt(),
                               message.substring(7, 10).toInt(),
                               message.substring(10, 13).toInt(),
                               message.substring(13, 16).toInt() };
    unsigned char end[4] = { message.substring(17, 20).toInt(),
                             message.substring(20, 23).toInt(),
                             message.substring(23, 26).toInt(),
                             message.substring(26, 29).toInt() };
    walk(start, end);
  } else if (message.startsWith("TEST")) {
    test();
  } else if (message.startsWith("ADD_PAT")) {
    unsigned char pattern = message.substring(7, 8).toInt();
    unsigned char rgbw[4] = { message.substring(8, 11).toInt(),
                              message.substring(11, 14).toInt(),
                              message.substring(14, 17).toInt(),
                              message.substring(17, 20).toInt() };
    unsigned start = message.substring(20, 23).toInt();
    unsigned end = message.substring(23, 26).toInt();
    add_pattern(pattern, rgbw, start, end);
  } else if (message.startsWith("SHOW_PAT")) {
    show_pattern();
  }
  esp_send_data(pipe, "DONE");
}

void on_esp_message(const String &message) {
  bool handled = false;
  int indexBuffer = -1;

  if (STR_HAS(message, "WIFI DISCONNECT")) {
    handled = true;
    hasWifi = false;
    hasIP = false;
    Serial.println(F("wifi: LOST"));
  }
  if (STR_HAS(message, "WIFI CONNECTED")) {
    handled = true;
    hasWifi = true;
    LOG_LN("WIFI: YES");
  }
  if (STR_HAS(message, "WIFI GOT IP")) {
    handled = true;
    hasIP = true;
    Serial.println(F("IP: YES"));
  }
  if (STR_HAS(message, ",CONNECT")) {
    handled = true;
    LOG_LN("New connection");
  }
  if (STR_HAS(message, ",CLOSED")) {
    handled = true;
    LOG_LN("New connection");
  }

  indexBuffer = message.indexOf("+IPD");
  if (indexBuffer > 0) {
    handled = true;
    LOG_LN("TCP message");
    on_network_message(message.substring(indexBuffer));
  }

  if (STR_HAS(message, "ready")) {
    LOG_LN("Reset detected, reinitializing...");
    esp_setup_server();
  }

  if (!handled) {
    LOG_LN("Unknow message:");
    LOGV_LN(message);
  }
}

void loop() {
  while (espSerial.available()) {
    on_esp_message(esp_read());
  }
  while (Serial.available()) espSerial.write(Serial.read());

  tick_animation();
  delay(1);
}
